﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Projet_rpg
{
    class Model
    {
        #region privates attributes
        #endregion privates attributes

        #region accessors
        #endregion accessors

        #region constructor
        public Model()
        {

        }
        #endregion constructor

        #region public methods
        /// <summary>
        /// Fonction qui affiche la map
        /// </summary>
        /// <param name="fightMap"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public View CreateFightMap(View fightMap, string fileName)
        {
            //Sélectionne l'image qui porte le nom envoyé en paramètre
            fightMap.BackgroundImage = (Bitmap)Projet_rpg.Properties.Resources.ResourceManager.GetObject(fileName);
            fightMap.FormBorderStyle = FormBorderStyle.None;
            fightMap.Size = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            fightMap.BackgroundImageLayout = ImageLayout.Stretch;

            return fightMap;
        }
        #endregion public methods

        #region privates methods
        #endregion privates methods
    }
}
