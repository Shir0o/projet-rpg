﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_rpg
{
    class CharacterClass
    {
        #region privates attributes
        private string _className;
        private int _move;
        #endregion privates attributes

        #region accessors
        #endregion accessors

        #region constructor
        public CharacterClass(string className, int move)
        {
            _className = className;
            _move = move;
        }
        #endregion constructor

        #region public methods
        #endregion public methods

        #region privates methods
        #endregion privates methods
    }
}
