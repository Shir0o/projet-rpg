﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_rpg
{
    class Character
    {
        #region privates attributes
        private List<Items> _bag;
        private int _idCharacter;
        private string _name;
        private int _level;
        private int _experience;
        private int _maxHealPoint;
        private int _actualHealPoint;
        private int _strenght;
        private int _intelligence;
        private int _defense;
        private int _resistance;
        private int _luck;
        private int _speed;
        private int[] _skill;
        private bool _alive;
        private string _className;
        #endregion privates attributes

        #region accessors
        /// <summary>
        /// Retourne l'id du personnage
        /// </summary>
        /// <returns></returns>
        public int idCharacter()
        {
            return _idCharacter;
        }

        /// <summary>
        /// Retourne le nom du personnage
        /// </summary>
        /// <returns></returns>
        public string name()
        {
            return _name;
        }

        /// <summary>
        /// Retourne le niveau du personnage
        /// </summary>
        /// <returns></returns>
        public int level()
        {
            return _level;
        }

        /// <summary>
        /// Retourne les points d'exp du personnage
        /// </summary>
        /// <returns></returns>
        public int experience()
        {
            return _experience;
        }

        /// <summary>
        /// Retourne les points de vies maximum du personnages
        /// </summary>
        /// <returns></returns>
        public int maxHealPoint()
        {
            return _maxHealPoint;
        }

        /// <summary>
        /// Retourne les points de vies actuel du personnage
        /// </summary>
        /// <returns></returns>
        public int actualHealPoint()
        {
            return _actualHealPoint;
        }

        /// <summary>
        /// Retourne le force du personnage
        /// </summary>
        /// <returns></returns>
        public int strength()
        {
            return _strenght;
        }

        /// <summary>
        /// Retourne l'intelligence du personnage
        /// </summary>
        /// <returns></returns>
        public int intelligence()
        {
            return _intelligence;
        }

        /// <summary>
        /// Retourne le défense du personnage
        /// </summary>
        /// <returns></returns>
        public int defense()
        {
            return _defense;
        }

        /// <summary>
        /// Retourne le résistance du personnage
        /// </summary>
        /// <returns></returns>
        public int resistance()
        {
            return _resistance;
        }

        /// <summary>
        /// Retourne le chance du personnage
        /// </summary>
        /// <returns></returns>
        public int luck()
        {
            return _luck;
        }

        /// <summary>
        /// Retourne la vitesse du personnage
        /// </summary>
        /// <returns></returns>
        public int speed()
        {
            return _speed;
        }

        /// <summary>
        /// Retourne les compétences du personnage
        /// </summary>
        /// <returns></returns>
        public int[] skill()
        {
            return _skill;
        }

        /// <summary>
        /// Retourne l'indication que le personnage soit vivant ou mort
        /// </summary>
        /// <returns></returns>
        public bool alive()
        {
            return _alive;
        }

        /// <summary>
        /// Retourne le nom de la classe du personnage
        /// </summary>
        /// <returns></returns>
        public string className()
        {
            return _className;
        }
        #endregion accessors

        #region constructor
        public Character(int idCharacter, string name, int level, int experience, int maxHealPoint, int strenght, int intelligence, int defense, int resistance, int luck, int speed, int[] skill, bool alive, string className)
        {
            _idCharacter = idCharacter;
            _name = name;
            _level = level;
            _experience = experience;
            _maxHealPoint = maxHealPoint;
            _actualHealPoint = maxHealPoint;
            _strenght = strenght;
            _intelligence = intelligence;
            _defense = defense;
            _resistance = resistance;
            _luck = luck;
            _speed = speed;
            _skill = skill;
            _alive = alive;
            _className = className;
        }
        #endregion constructor

        #region public methods
        /// <summary>
        /// Ajout d'un objet dans le sac du personnage
        /// </summary>
        /// <param name="idItems"></param>
        /// <param name="name"></param>
        /// <param name="effect"></param>
        /// <param name="description"></param>
        /// <param name="minRange"></param>
        /// <param name="maxRange"></param>
        /// <param name="price"></param>
        /// <param name="icon"></param>
        /// <param name="durability"></param>
        /// <param name="speed"></param>
        /// <param name="luck"></param>
        /// <param name="requiredSkill"></param>
        public void AddItem(int idItems, string name, int effect, string description, int minRange, int maxRange, int price, string icon, int durability, int speed = -1, int luck = -1, string requiredSkill = null)
        {
            _bag.Add(new Items(idItems, name, effect, description, minRange, maxRange, price, icon, durability, speed, luck, requiredSkill));
        }

        /// <summary>
        /// Supprime l'item qui porte l'id entré en paramètre
        /// </summary>
        /// <param name="idItems"></param>
        public void DeleteItem(int idItems)
        {
            int index = 0;

            //Recherche quel item porte l'id entré en paramètre
            foreach (Items item in _bag)
            {
                if (item.idItems() == idItems)
                {
                    _bag.RemoveAt(index);
                    break;
                }
                index++;
            }
        }
        #endregion public methods

        #region privates methods
        #endregion privates methods
    }
}
