﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projet_rpg
{
    class Controller
    {
        #region privates attributes
        private Model _model;
        private View _view;
        private Database _database;
        private Character[] _character;
        private Trader _trader;
        private Reserve _reserve;
        private List<CharacterClass> _characterClass;
        #endregion privates attributes

        #region accessors
        #endregion accessors

        #region constructor
        public Controller()
        {
            _model = new Model();
            _database = new Database("Test");
            View test = new View();

            test = _model.CreateFightMap(test, _database.SelectMapImage(1));

            //Test
            Application.Run(test);
        }
        #endregion constructor

        #region public methods
        #endregion public methods

        #region privates methods
        #endregion privates methods
    }
}
