﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Projet_rpg.Properties {
    using System;
    
    
    /// <summary>
    ///   Une classe de ressource fortement typée destinée, entre autres, à la consultation des chaînes localisées.
    /// </summary>
    // Cette classe a été générée automatiquement par la classe StronglyTypedResourceBuilder
    // à l'aide d'un outil, tel que ResGen ou Visual Studio.
    // Pour ajouter ou supprimer un membre, modifiez votre fichier .ResX, puis réexécutez ResGen
    // avec l'option /str ou régénérez votre projet VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Retourne l'instance ResourceManager mise en cache utilisée par cette classe.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Projet_rpg.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Remplace la propriété CurrentUICulture du thread actuel pour toutes
        ///   les recherches de ressources à l'aide de cette classe de ressource fortement typée.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Recherche une ressource localisée de type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Arene {
            get {
                object obj = ResourceManager.GetObject("Arene", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à DROP TABLE IF EXISTS &quot;Skills&quot;;
        ///DROP TABLE IF EXISTS &quot;Classe&quot;;
        ///DROP TABLE IF EXISTS &quot;Classe_Skill&quot;;
        ///DROP TABLE IF EXISTS &quot;Characters&quot;;
        ///DROP TABLE IF EXISTS &quot;Character_Skill&quot;;
        ///DROP TABLE IF EXISTS &quot;RelationShip&quot;;
        ///DROP TABLE IF EXISTS &quot;Items&quot;;
        ///DROP TABLE IF EXISTS &quot;Character_bag&quot;;
        ///DROP TABLE IF EXISTS &quot;Reserve&quot;;
        ///DROP TABLE IF EXISTS &quot;Trader&quot;;
        ///DROP TABLE IF EXISTS &quot;Trader_Items&quot;;
        ///DROP TABLE IF EXISTS &quot;Map&quot;;
        ///DROP TABLE IF EXISTS &quot;Chest&quot;;
        ///DROP TABLE IF EXISTS &quot;Ennemies&quot;;
        ///DROP TABLE IF EXISTS &quot;Items_ [le reste de la chaîne a été tronqué]&quot;;.
        /// </summary>
        internal static string Script {
            get {
                return ResourceManager.GetString("Script", resourceCulture);
            }
        }
    }
}
