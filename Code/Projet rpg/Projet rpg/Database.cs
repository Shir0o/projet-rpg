﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace Projet_rpg
{
    class Database
    {
        #region privates attributes
        private SQLiteConnection _dbConnection;
        private SQLiteDataReader _reader;
        private string _dbDirLocation;
        #endregion privates attributes

        #region accessors
        #endregion accessors

        #region constructor
        public Database(string dbName)
        {
            dbName += ".sqlite";
            _dbDirLocation = AppDomain.CurrentDomain.BaseDirectory + @"\Sauvegardes\";
            string dbLocation = _dbDirLocation + dbName;
            _dbConnection = new SQLiteConnection("Data Source=" + dbLocation + ";Version=3;");

            if (!Directory.Exists(_dbDirLocation)) Directory.CreateDirectory(_dbDirLocation);
            if (!File.Exists(dbLocation))
            {
                string script = Projet_rpg.Properties.Resources.Script.ToString();
                SQLiteConnection.CreateFile(dbLocation);
                SQLiteCommand command = new SQLiteCommand(script, _dbConnection);
                _dbConnection.Open();
                command.ExecuteNonQuery();
                _dbConnection.Close();
            }
        }
        #endregion constructor

        #region public methods
        /// <summary>
        /// Fonction qui va chercher le nom de la map correspondant à l'id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string SelectMapImage(int id)
        {
            SQLiteCommand command = new SQLiteCommand("SELECT FileName FROM Map WHERE idMap = " + id.ToString(), _dbConnection);
            _dbConnection.Open();
            _reader = command.ExecuteReader();

            string fileName = "";

            while(_reader.Read())
            {
                fileName = _reader["FileName"].ToString();
            }
            command.Dispose();
            _dbConnection.Close();

            return fileName;
        }
        #endregion public methods

        #region privates methods
        #endregion privates methods
    }
}
