﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_rpg
{
    class Items
    {
        #region privates attributes
        private int _idItems;
        private string _name;
        private int _effect;
        private string _description;
        private int _minRange;
        private int _maxRange;
        private int _price;
        private string _icon;
        private int _durability;
        private int _speed;
        private int _luck;
        private string _requiredSkill;
        #endregion privates attributes

        #region accessors
        /// <summary>
        /// Retourne l'index de l'item
        /// </summary>
        /// <returns></returns>
        public int idItems()
        {
            return _idItems;
        }

        /// <summary>
        /// Retourne le nom de l'objet
        /// </summary>
        /// <returns></returns>
        public string name()
        {
            return _name;
        }

        /// <summary>
        /// Retourne l'effet de l'objet
        /// </summary>
        /// <returns></returns>
        public int effect()
        {
            return _effect;
        }

        /// <summary>
        /// Retourne le porté minimal de l'objet
        /// </summary>
        /// <returns></returns>
        public int minRange()
        {
            return _minRange;
        }

        /// <summary>
        /// Retourne la portée maximale de l'objet
        /// </summary>
        /// <returns></returns>
        public int maxRange()
        {
            return _maxRange;
        }

        /// <summary>
        /// Retourne le prix de l'objet
        /// </summary>
        /// <returns></returns>
        public int price()
        {
            return _price;
        }

        /// <summary>
        /// Retourne le nom de l'icône pour l'objet
        /// </summary>
        /// <returns></returns>
        public string icon()
        {
            return _icon;
        }

        /// <summary>
        /// Retourne la durabilité de l'objet
        /// </summary>
        /// <returns></returns>
        public int durability()
        {
            return _durability;
        }

        /// <summary>
        /// Retourne la vitesse octroyé par l'objet
        /// </summary>
        /// <returns></returns>
        public int speed()
        {
            return _speed;
        }

        /// <summary>
        /// Retourne la chance octroyé par l'objet
        /// </summary>
        /// <returns></returns>
        public int luck()
        {
            return _luck;
        }

        /// <summary>
        /// Retourne le skill requis pour l'objet
        /// </summary>
        /// <returns></returns>
        public string requiredSkill()
        {
            return _requiredSkill;
        }
        #endregion accessors

        #region constructor
        public Items(int idItems, string name, int effect, string description, int minRange, int maxRange, int price, string icon, int durability, int speed = -1, int luck = -1, string requiredSkill = null)
        {
            _idItems = idItems;
            _name = name;
            _effect = effect;
            _description = description;
            _minRange = minRange;
            _maxRange = maxRange;
            _price = price;
            _icon = icon;
            _durability = durability;
            _speed = speed;
            _luck = luck;
            _requiredSkill = requiredSkill;
        }
        #endregion constructor

        #region public methods
        #endregion public methods

        #region privates methods
        #endregion privates methods
    }
}
