﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_rpg
{
    class Trader
    {
        #region privates attributes
        private List<Items> _items;
        private string _name;
        #endregion privates attributes

        #region accessors
        /// <summary>
        /// Retourne la liste d'objets en vente par le marchand
        /// </summary>
        /// <returns></returns>
        public List<Items> items()
        {
            return _items;
        }

        /// <summary>
        /// Retourne le nom du marchand
        /// </summary>
        /// <returns></returns>
        public string name()
        {
            return _name;
        }
        #endregion accessors

        #region constructor
        public Trader(string name)
        {
            _name = name;
        }
        #endregion constructor

        #region public methods
        /// <summary>
        /// Ajout d'un objet dans le sac du marchand
        /// </summary>
        /// <param name="idItems"></param>
        /// <param name="name"></param>
        /// <param name="effect"></param>
        /// <param name="description"></param>
        /// <param name="minRange"></param>
        /// <param name="maxRange"></param>
        /// <param name="price"></param>
        /// <param name="icon"></param>
        /// <param name="durability"></param>
        /// <param name="speed"></param>
        /// <param name="luck"></param>
        /// <param name="requiredSkill"></param>
        public void AddItem(int idItems, string name, int effect, string description, int minRange, int maxRange, int price, string icon, int durability, int speed = -1, int luck = -1, string requiredSkill = null)
        {
            _items.Add(new Items(idItems, name, effect, description, minRange, maxRange, price, icon, durability, speed, luck, requiredSkill));
        }
        #endregion public methods

        #region privates methods
        #endregion privates methods
    }
}
