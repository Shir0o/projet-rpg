DROP TABLE IF EXISTS "Skills";
DROP TABLE IF EXISTS "Classe";
DROP TABLE IF EXISTS "Classe_Skill";
DROP TABLE IF EXISTS "Characters";
DROP TABLE IF EXISTS "Character_Skill";
DROP TABLE IF EXISTS "RelationShip";
DROP TABLE IF EXISTS "Items";
DROP TABLE IF EXISTS "Character_bag";
DROP TABLE IF EXISTS "Reserve";
DROP TABLE IF EXISTS "Trader";
DROP TABLE IF EXISTS "Trader_Items";
DROP TABLE IF EXISTS "Map";
DROP TABLE IF EXISTS "Chest";
DROP TABLE IF EXISTS "Ennemies";
DROP TABLE IF EXISTS "Items_Player";
DROP TABLE IF EXISTS "Player";

CREATE TABLE "Skills" (
	"idSkills"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"Name"	TEXT NOT NULL
);

CREATE TABLE "Classe" (
	"idClasse"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"Name"	TEXT NOT NULL,
	"Move" INTEGER NOT NULL
);

CREATE TABLE "Classe_Skill" (
	"idClasse_Skill" INTEGER PRIMARY KEY AUTOINCREMENT,
	"MaxSkills"	INTEGER NOT NULL,
	"FkClasse"	INTEGER NOT NULL,
	"FkSkills"	INTEGER NOT NULL,
	FOREIGN KEY(FkClasse) REFERENCES Classe(idClasse),
	FOREIGN KEY(FkSkills) REFERENCES Skills(idSkills)
);

CREATE TABLE "Characters" (
	"idCharacter" INTEGER PRIMARY KEY AUTOINCREMENT,
	"Name"	TEXT NOT NULL,
	"Level"	INTEGER NOT NULL,
	"Experience" INTEGER NOT NULL,
	"HealPoint"	INTEGER NOT NULL,
	"Strength"	INTEGER NOT NULL,
	"Intelligence"	INTEGER NOT NULL,
	"Defense"	INTEGER NOT NULL,
	"Resistance"	INTEGER NOT NULL,
	"Luck"	INTEGER NOT NULL,
	"Speed"	INTEGER NOT NULL,
	"Unlock"	BOOLEAN NOT NULL,
	"Alive" BOOLEAN NOT NULL,
	"FkClasse"	INTEGER NOT NULL,
	FOREIGN KEY(FkClasse) REFERENCES Classe(idClasse)
);

CREATE TABLE "Character_Skill" (
	"idCharacter_Skill"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"ActualSkill"	INTEGER NOT NULL,
	"FkCharacter"	INTEGER NOT NULL,
	"FkSkill"	TEXT NOT NULL,
	FOREIGN KEY(FkCharacter) REFERENCES Characters(idCharacter),
	FOREIGN KEY(FkSkill) REFERENCES Skills(idSkills)
);

CREATE TABLE "RelationShip" (
	"idRelationShip" INTEGER PRIMARY KEY AUTOINCREMENT,
	"RelationValue"	INTEGER NOT NULL,
	"FkCharacter1"	INTEGER NOT NULL,
	"FkCharacter2"	INTEGER NOT NULL,
	FOREIGN KEY(FkCharacter1) REFERENCES Characters(idCharacter),
	FOREIGN KEY(FkCharacter2) REFERENCES Characters(idCharacter)
);

CREATE TABLE "Items" (
	"idItems" INTEGER PRIMARY KEY AUTOINCREMENT,
	"Name"	TEXT NOT NULL,
	"Description"	TEXT NOT NULL,
	"Effect"	INTEGER NOT NULL,
	"InitialDurability"	INTEGER NOT NULL,
	"Speed"	INTEGER,
	"Luck"	INTEGER,
	"MinRange" INTEGER NOT NULL,
	"MaxRange" INTEGER NOT NULL,
	"Price" INTEGER NOT NULL,
	"Icon" TEXT NOT NULL,
	"FkSkills"	INTEGER NOT NULL,
	FOREIGN KEY(FkSkills) REFERENCES Skills(idSkills)
);

CREATE TABLE "Items_Player" (
	"idItems_Player" INTEGER PRIMARY KEY AUTOINCREMENT,
	"ActualDurability" INTEGER NOT NULL,
	"FkItems" INTEGER NOT NULL,
	FOREIGN KEY(FkItems) REFERENCES Items(idItems)
);

CREATE TABLE "Character_bag" (
	"idCharacter_Bag" INTEGER PRIMARY KEY AUTOINCREMENT,
	"Lootable" BOOLEAN NOT NULL,
	"FkCharacter"	INTEGER NOT NULL,
	"FkItemsPlayer"	INTEGER NOT NULL,
	FOREIGN KEY(FkCharacter) REFERENCES Characters(idCharacter),
	FOREIGN KEY(FkItemsPlayer) REFERENCES Items_Player(idItemsPlayer)
);

CREATE TABLE "Reserve" (
	"idReserve"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"FkItemsPlayer"	INTEGER NOT NULL,
	FOREIGN KEY(FkItemsPlayer) REFERENCES Items_Player(idItemsPlayer)
);

CREATE TABLE "Trader" (
	"idTrader" INTEGER PRIMARY KEY AUTOINCREMENT,
	"Name" TEXT NOT NULL
);

CREATE TABLE "Trader_Items" (
	"idTrader" INTEGER PRIMARY KEY AUTOINCREMENT,
	"FkItems" INTEGER NOT NULL,
	"FkTrader" INTEGER NOT NULL,
	FOREIGN KEY(FkItems) REFERENCES Items(idItems),
	FOREIGN KEY(FkTrader) REFERENCES Trader(idTrader)
);

CREATE TABLE "Map" (
	"idMap" INTEGER PRIMARY KEY AUTOINCREMENT,
	"FileName" TEXT NOT NULL
);

CREATE TABLE "Chest" (
	"idChest" INTEGER PRIMARY KEY AUTOINCREMENT,
	"LocationX" INTEGER NOT NULL,
	"LocationY" INTEGER NOT NULL,
	"FkItems" INTEGER NOT NULL,
	"FkMap" INTEGER NOT NULL,
	FOREIGN KEY(FkItems) REFERENCES Items(idItems),
	FOREIGN KEY(FkMap) REFERENCES Map(idMap)
);

CREATE TABLE "Ennemies" (
	"idEnnemies" INTEGER PRIMARY KEY AUTOINCREMENT,
	"LocationX" INTEGER NOT NULL,
	"LocationY" INTEGER NOT NULL,
	"FkCharacter" INTEGER NOT NULL,
	"FkMap" INTEGER NOT NULL,
	FOREIGN KEY(FkCharacter) REFERENCES Characters(idCharacter),
	FOREIGN KEY(FkMap) REFERENCES Map(idMap)
);

CREATE TABLE "Player" (
	"idPlayer" INTEGER PRIMARY KEY AUTOINCREMENT,
	"Coins" INTEGER NOT NULL
);

INSERT INTO Map(FileName) VALUES("Arene");