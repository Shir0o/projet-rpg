﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_rpg
{
    class Reserve
    {
        #region privates attributes
        private List<Items> _items;
        private int _coins;
        #endregion privates attributes

        #region accessors
        /// <summary>
        /// Retourne les objets de la réseve
        /// </summary>
        /// <returns></returns>
        public List<Items> items()
        {
            return _items;
        }

        /// <summary>
        /// Retourne le nombre de pièces du joueur
        /// </summary>
        /// <returns></returns>
        public int coins()
        {
            return _coins;
        }
        #endregion accessors

        #region constructor
        public Reserve(int coins = 100)
        {
            _coins = coins;
        }
        #endregion constructor

        #region public methods
        /// <summary>
        /// Ajout d'un objet dans la réserve
        /// </summary>
        /// <param name="idItems"></param>
        /// <param name="name"></param>
        /// <param name="effect"></param>
        /// <param name="description"></param>
        /// <param name="minRange"></param>
        /// <param name="maxRange"></param>
        /// <param name="price"></param>
        /// <param name="icon"></param>
        /// <param name="durability"></param>
        /// <param name="speed"></param>
        /// <param name="luck"></param>
        /// <param name="requiredSkill"></param>
        public void AddItem(int idItems, string name, int effect, string description, int minRange, int maxRange, int price, string icon, int durability, int speed = -1, int luck = -1, string requiredSkill = null)
        {
            _items.Add(new Items(idItems, name, effect, description, minRange, maxRange, price, icon, durability, speed, luck, requiredSkill));
        }

        /// <summary>
        /// Supprime l'item qui porte l'id entré en paramètre
        /// </summary>
        /// <param name="idItems"></param>
        public void DeleteItem(int idItems)
        {
            int index = 0;

            //Recherche quel item porte l'id entré en paramètre
            foreach(Items item in _items)
            {
                if(item.idItems() == idItems)
                {
                    _items.RemoveAt(index);
                    break;
                }
                index++;
            }
        }

        /// <summary>
        /// Ajout de pièces d'or
        /// </summary>
        /// <param name="nbCoins"></param>
        public void AddCoins(int nbCoins)
        {
            _coins += nbCoins;
        }

        /// <summary>
        /// Si possible, on retire le nombre de pièces d'or entré en paramètre
        /// </summary>
        /// <param name="nbCoins"></param>
        /// <returns></returns>
        public bool DeleteCoins(int nbCoins)
        {
            //Si le joueur à la somme nécessaire, on la déduit
            if(_coins >= nbCoins)
            {
                _coins -= nbCoins;
                return true;
            }
            //Sinon, on retourne une erreur
            else
            {
                return false;
            }
        }
        #endregion public methods

        #region privates methods
        #endregion privates methods
    }
}
